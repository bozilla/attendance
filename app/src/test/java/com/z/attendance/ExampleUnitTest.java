package com.z.attendance;

import com.google.gson.Gson;
import com.z.attendance.models.EmployeeData;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void employeeData_fromJson(){
        String json = " [{\"id\":\"2\",\"name\":\"Zach\",\"surname\":\"Bolin\",\"age\":\"6\",\"username\":\"123\",\"password\":\"123\"}]";
        EmployeeData[] data = new Gson().fromJson(json, EmployeeData[].class);
        List<EmployeeData> dataList = Arrays.asList(data);
        System.out.print(dataList.get(0).getName());
        assertEquals(true,true);
    }
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}