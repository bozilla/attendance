package com.z.attendance.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.z.attendance.Keys;
import com.z.attendance.R;
import com.z.attendance.models.Clan;
import com.z.attendance.models.Feed;
import com.z.attendance.models.War;
import com.z.attendance.services.CyaServiceImpl;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedFragment extends Fragment {


    public FeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_feed, container, false);
        TextView textView = (TextView) view.findViewById(R.id.textView);
        textView.setText(getClan().getName() + " Key: " + getClan().getK3y());


        // Get ListView object from xml
        ListView listView  = (ListView) view.findViewById(R.id.list);
        Button feedButton = (Button) view.findViewById(R.id.feedBtn);
//        feedButton.setText("?");



        CyaServiceImpl svc = new CyaServiceImpl(view.getContext());
        try {
            War w = svc.getWar(getClan().getCurrentWarId());
            feedButton.setText(String.valueOf(w.getNumBases()));
            svc = new CyaServiceImpl(view.getContext());
            List<Feed> feed = svc.getFeeds(getClan().getCurrentWarId());
            String[] values = new String[feed.size()];

            for(int i = 0 ; i <feed.size(); i++)
            {
                values[i] = feed.get(i).getText();
            }


            // Define a new Adapter
            // First parameter - Context
            // Second parameter - Layout for the row
            // Third parameter - ID of the TextView to which the data is written
            // Forth - the Array of data

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, values);


            // Assign adapter to ListView
            listView.setAdapter(adapter);



        } catch(Exception e)
        {
            Log.e("Z", "error getting feed");
        }
        return view;
    }

    private Clan getClan() {
        Gson gson = new Gson();
        SharedPreferences prefs = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        String json = prefs.getString(Keys.CLAN,"");
        return gson.fromJson(json, Clan.class);
    }



}
