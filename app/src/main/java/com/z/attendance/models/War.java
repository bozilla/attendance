package com.z.attendance.models;

/**
 * Created by Z on 7/24/2017.
 */

public class War extends BaseModel{

    int id;
    String against;
    int numBases;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAgainst() {
        return against;
    }

    public void setAgainst(String against) {
        this.against = against;
    }

    public int getNumBases() {
        return numBases;
    }

    public void setNumBases(int numBases) {
        this.numBases = numBases;
    }
}
