package com.z.attendance.models;

import com.google.gson.Gson;

/**
 * Created by Z on 8/1/2017.
 */

public class BaseModel {

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public String toString() {
        return toJson();
    }
}
