package com.z.attendance.models;

/**
 * Created by Z on 7/24/2017.
 */

public class Clan extends BaseModel{

    int id;
    String name;
    int currentWarId;
    String k3y;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentWarId() {
        return currentWarId;
    }

    public void setCurrentWarId(int currentWarId) {
        this.currentWarId = currentWarId;
    }

    public String getK3y() {
        return k3y;
    }

    public void setK3y(String k3y) {
        this.k3y = k3y;
    }
}
