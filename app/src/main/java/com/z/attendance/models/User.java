package com.z.attendance.models;

/**
 * Created by Z on 7/24/2017.
 */

public class User extends BaseModel {




    String facebookId;
    String name;
    int clanId;
    int isAdmin;
    int numCallsThisWar;

    public User() {
        isAdmin = 0;
        numCallsThisWar = 0;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getClanId() {
        return clanId;
    }

    public void setClanId(int clanId) {
        this.clanId = clanId;
    }

    public int isAdmin() {
        return isAdmin;
    }

    public void setAdmin(int admin) {
        isAdmin = admin;
    }

    public int getNumCallsThisWar() {
        return numCallsThisWar;
    }

    public void setNumCallsThisWar(int numCallsThisWar) {
        this.numCallsThisWar = numCallsThisWar;
    }


}
