package com.z.attendance.models;

/**
 * Created by Z on 7/24/2017.
 */

public class Feed extends BaseModel{
    int id;
    int warId;
    String text;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getWarId() {
        return warId;
    }

    public void setWarId(int warId) {
        this.warId = warId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
