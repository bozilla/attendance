package com.z.attendance;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.z.attendance.adapters.NavDrawerAdapter;
import com.z.attendance.fragments.ClanFragment;
import com.z.attendance.fragments.FeedFragment;
import com.z.attendance.fragments.Layout10Fragment;
import com.z.attendance.fragments.Layout15Fragment;
import com.z.attendance.fragments.Layout20Fragment;
import com.z.attendance.fragments.Layout25Fragment;
import com.z.attendance.fragments.Layout30Fragment;
import com.z.attendance.fragments.Layout35Fragment;
import com.z.attendance.fragments.Layout40Fragment;
import com.z.attendance.fragments.Layout45Fragment;
import com.z.attendance.fragments.Layout50Fragment;
import com.z.attendance.models.Clan;
import com.z.attendance.models.DrawerItem;
import com.z.attendance.models.Feed;
import com.z.attendance.models.User;
import com.z.attendance.services.CyaServiceImpl;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    public String HEADER_NAME = "Zach Bolin";
    public String HEADER_EMAIL = "zdbolin@gmail.com";
    public int HEADER_IMAGE = R.drawable.rams;

    private final static int CLAN_FRAGMENT = 1;
    private static final int FEED_FRAGMENT = 2;
    private static final int FEED_FRAGMENT_BACK = 3;
    private static final int LAYOUT_10_FRAGMENT = 10;
    private static final int LAYOUT_15_FRAGMENT = 15;
    private final static int LAYOUT_20_FRAGMENT = 20;
    private final static int LAYOUT_25_FRAGMENT = 25;
    private final static int LAYOUT_30_FRAGMENT = 30;
    private final static int LAYOUT_35_FRAGMENT = 35;
    private final static int LAYOUT_40_FRAGMENT = 40;
    private final static int LAYOUT_45_FRAGMENT = 45;
    private final static int LAYOUT_50_FRAGMENT = 50;
    private final static int PREFERENCE_ACTIVITY = 12;



    private static final String CLAN = "Clan";
    private static final String FEED = "Feed";
    private static final String CALL_YOUR_ATTACKS = "Call Your Attacks";
    private static final String SETTINGS = "Settings";
    private CallbackManager callbackManager;
    private TextView textView, textView2;
    private EditText clanInput;
    private Button simLogInBtn, simLogOutBtn, findMyClanBtn, addMyClanBtn, doClanInputBtn, cancelClanBtn, feedBtn;
    private LoginButton loginButton;
    private int currentFragment = FEED_FRAGMENT;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawerLayout Drawer;
    private RecyclerView.Adapter mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private List<DrawerItem> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView= (TextView) findViewById(R.id.textView);
        textView2= (TextView) findViewById(R.id.textView2);

        clanInput = (EditText) findViewById(R.id.clanInput);

        simLogInBtn = (Button) findViewById(R.id.btnSimulateLoginSuccess);
        simLogOutBtn = (Button) findViewById(R.id.btnSimulateLogoutSuccess);
        findMyClanBtn = (Button) findViewById(R.id.findMyClan);
        addMyClanBtn = (Button) findViewById(R.id.addMyClan);
        doClanInputBtn = (Button) findViewById(R.id.doClanInput);
        cancelClanBtn = (Button) findViewById(R.id.cancelClan);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);
        dataList = new ArrayList<DrawerItem>();
        addItemsToDataList();

        mAdapter = new NavDrawerAdapter(dataList, this, HEADER_NAME, HEADER_EMAIL, HEADER_IMAGE);

        mRecyclerView.setAdapter(mAdapter);

        final GestureDetector mGestureDetector =
                new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });

        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if (child != null && mGestureDetector.onTouchEvent(motionEvent)){
                    Drawer.closeDrawers();
                    onTouchDrawer(recyclerView.getChildLayoutPosition(child));
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        FacebookSdk.sdkInitialize(getApplicationContext());
        loginButton = (LoginButton) findViewById(R.id.fb_login_bn);

        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("Z", "onSuccess");
//                tv.setText("Login Success \n" + Profile.getCurrentProfile().getFirstName() + "\n"+loginResult.getAccessToken().getUserId() + "\n" + loginResult.getAccessToken().getToken());
                    textView.setText("loged in " + loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {

                Log.e("Z", "onCancel");
                textView.setText("Login cancelled");
            }

            @Override
            public void onError(FacebookException error) {

                Log.e("Z", "onError");
                textView.setText("Error");
            }
        });
//        onTouchDrawer(currentFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openFragment(final Fragment fragment) {

            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left)
                    .replace(R.id.container, fragment)
                    .commit();


    }

    public void openFragmentReverse(final Fragment fragment) {

        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
                .replace(R.id.container, fragment)
                .commit();


    }



    private void onTouchDrawer(final int position) {
        currentFragment = position;
        switch (position) {
            case CLAN_FRAGMENT:
                openFragment(new ClanFragment());
                setTitle(CLAN);
                break;
            case FEED_FRAGMENT:
                openFragment(new FeedFragment());
                setTitle(FEED);
                reFindShit();
                textView.setText(FEED);
                break;
            case FEED_FRAGMENT_BACK:
                openFragmentReverse(new FeedFragment());
                setTitle(FEED);
                reFindShit();
                textView.setText(FEED);
                break;
            case LAYOUT_10_FRAGMENT:
                openFragment(new Layout10Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_15_FRAGMENT:
                openFragment(new Layout15Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_20_FRAGMENT:
                openFragment(new Layout20Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_25_FRAGMENT:
                openFragment(new Layout25Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_30_FRAGMENT:
                openFragment(new Layout30Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_35_FRAGMENT:
                openFragment(new Layout35Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_40_FRAGMENT:
                openFragment(new Layout40Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_45_FRAGMENT:
                openFragment(new Layout45Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case LAYOUT_50_FRAGMENT:
                openFragment(new Layout50Fragment());
                setTitle(CALL_YOUR_ATTACKS);
                break;
            case PREFERENCE_ACTIVITY:
                startActivity(new Intent(this, PreferenceActivity.class));
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                setTitle(SETTINGS);
                break;
            default:
                return;
        }
    }

    public void setTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    private void addItemsToDataList() {

        dataList.add(new DrawerItem(CLAN,  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem(FEED,  R.drawable.ic_settings_black_24dp));

        dataList.add(new DrawerItem("10",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("15",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("20",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("25",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("30",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("35",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("40",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("45",  R.drawable.ic_settings_black_24dp));
        dataList.add(new DrawerItem("50",  R.drawable.ic_settings_black_24dp));

        dataList.add(new DrawerItem(SETTINGS,  R.drawable.ic_settings_black_24dp));
    }


    public void doLoginSuccess(View view)
    {



        mToolbar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        textView2.setVisibility(View.GONE);
        simLogInBtn.setVisibility(View.GONE);
        simLogOutBtn.setVisibility(View.GONE);
        try
        {
            CyaServiceImpl svc = new CyaServiceImpl(this);
            User u = svc.getUser("12");
            saveUser(u);
        }catch(Exception e) {
            Log.e("Z", e.getMessage());
        }
        onTouchDrawer(CLAN_FRAGMENT);

    }

    public void doLogoutSuccess(View view)
    {

        mToolbar.setVisibility(View.VISIBLE);

    }

    private void reFindShit()
    {
        findMyClanBtn = (Button) findViewById(R.id.findMyClan);
        addMyClanBtn = (Button) findViewById(R.id.addMyClan);
        doClanInputBtn = (Button) findViewById(R.id.doClanInput);
        cancelClanBtn = (Button) findViewById(R.id.cancelClan);
        clanInput = (EditText) findViewById(R.id.clanInput);
        feedBtn = (Button) findViewById(R.id.feedBtn);
    }

    public void saveClan(Clan clan)
    {
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(clan);
        editor.putString(Keys.CLAN, json);
        loginButton.setVisibility(View.INVISIBLE);
        editor.commit();
    }

    public void saveUser(User user)
    {
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(Keys.USER, json);
        loginButton.setVisibility(View.INVISIBLE);
        editor.commit();
    }

    private String getPref(String key)
    {
        SharedPreferences loginData = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String userId = loginData.getString(key, "");
        return userId;
    }





    public void doAddMyClan(View view)
    {
        reFindShit();
        cancelClanBtn.setVisibility(View.VISIBLE);
        doClanInputBtn.setText(getPref("userId"));
        findMyClanBtn.setVisibility(View.INVISIBLE);
        addMyClanBtn.setVisibility(View.INVISIBLE);
        clanInput.setVisibility(View.VISIBLE);
        doClanInputBtn.setVisibility(View.VISIBLE);
        Log.i("Z", "DO ADD MY CLAN");

        clanInput.setHint("Clan name");
    }




    public void doFindMyClan(View view)
    {
        reFindShit();
        clanInput.setVisibility(View.VISIBLE);
        doClanInputBtn.setText("Search");
        findMyClanBtn.setVisibility(View.INVISIBLE);
        addMyClanBtn.setVisibility(View.INVISIBLE);
        cancelClanBtn.setVisibility(View.VISIBLE);
        doClanInputBtn.setVisibility(View.VISIBLE);

        Log.i("Z", "DO FIND MY CLAN");
        clanInput.setHint("Clan key");

    }

    public void doClanInput(View view)
    {
        reFindShit();
        CyaServiceImpl svc = new CyaServiceImpl(this);
        if(doClanInputBtn.getText().toString().equals("Search"))
        {
            try {
                final Clan found = svc.getClan(clanInput.getText().toString());
                Log.i("Z", found.getName());

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        this);

                // set title
                alertDialogBuilder.setTitle(found.getName());

                // set dialog message
                alertDialogBuilder
                        .setMessage("Do you wish to join this clan?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                saveClan(found);
                              onTouchDrawer(FEED_FRAGMENT);
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }catch(Exception e)
            {
                Log.e("Z", e.getMessage());
            }

        }
        else {
            Clan clan = new Clan();
            clan.setName(clanInput.getText().toString());
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            clan.setK3y(randomUUIDString.substring(0, 8).toUpperCase());
            svc.putClan(clan);
            onTouchDrawer(FEED_FRAGMENT);
        }

    }
    public void feedBtnClicked(View view)
    {
         reFindShit();
         onTouchDrawer(Integer.parseInt(feedBtn.getText().toString()));
    }
    public void doClanCancel(View view)
    {
        reFindShit();
        cancelClanBtn.setVisibility(View.GONE);
        clanInput.setVisibility(View.GONE);
        doClanInputBtn.setVisibility(View.GONE);
        findMyClanBtn.setVisibility(View.VISIBLE);
        addMyClanBtn.setVisibility(View.VISIBLE);
        Log.i("Z", "cancle");
    }
    public void reusebackButtonnClicked(View view)
    {
        Log.i("Z", "reusebackButtonnClicked");
        onTouchDrawer(FEED_FRAGMENT_BACK);
    }
    public void townHallOneButtonClicked(View view)
    {
        Log.i("Z", "TH 1 CLICKED!!!!!!!!");

        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        String val = prefs.getString(Keys.CLAN, Keys.CLAN);
        String userVal = prefs.getString(Keys.USER, Keys.USER);
        Log.i("Z", val);
        Log.i("Z", userVal);
        try
        {
            JSONObject clan = new JSONObject(val);
            JSONObject user = new JSONObject(userVal);
            CyaServiceImpl svc = new CyaServiceImpl(this);
            Feed f  = new Feed();
            f.setText(user.getString("name") + " called #1");
            f.setWarId(Integer.parseInt(clan.getString("currentWarId")));
             svc.putFeed(f);
        } catch (Exception e )
        {

        }

    }
}
