package com.z.attendance.services;

import com.z.attendance.models.Base;
import com.z.attendance.models.Clan;
import com.z.attendance.models.Feed;
import com.z.attendance.models.User;
import com.z.attendance.models.War;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Z on 7/30/2017.
 */

public interface CyaService {

    public void deleteBase(Base base);

    public void putBase(Base base);

    public List<Base> getBases(int warId) throws InterruptedException , ExecutionException;

    public void putClan(Clan clan);

    public Clan getClan(int id) throws InterruptedException , ExecutionException;

    public Clan getClan(String k3y) throws InterruptedException , ExecutionException;

    public void deleteFeeds(int warId);

    public void putFeed(Feed feed);

    public List<Feed> getFeeds(int warId) throws InterruptedException , ExecutionException;

    public void putUser(User user);

    public User getUser(String id) throws InterruptedException , ExecutionException;

    public List<User> getUsers(int clanId) throws InterruptedException , ExecutionException;

    public void deleteWar(int id);

    public void putWar(War war);

    public War getWar(int id) throws InterruptedException , ExecutionException;
}
