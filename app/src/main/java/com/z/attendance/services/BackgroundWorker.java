package com.z.attendance.services;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by Z on 7/21/2017.
 */

public class BackgroundWorker extends AsyncTask<String, Void, String> {

    Context context;
    AlertDialog alertDialog;
    String json;

    BackgroundWorker(Context ctx) {
        context = ctx;
    }


    /**
     * @param params 1) url after ../cya/ e.g. clans.php?id=1
     *               2) HTTP verb e.g. GET
     *               3) Json String (optional)
     * @return
     */
    @Override
    protected String doInBackground(String... params) {
        String httpVerb = params[0];
        String urlAfterCya = params[1];

        String json = "";
        if (params.length >= 3) {
            json = params[2];
        }
//        String login_url = "http://52.60.159.159/cya/" + urlAfterCya;
        String login_url = "http://10.0.2.2/" + urlAfterCya;
        Log.i("Z", login_url);
        try {
            if(httpVerb.equals("GET"))
            {
              return doGet(login_url);
            }
            else if (httpVerb.equals("DELETE"))
            {
                Log.i("Z", "doing delete");
                HttpURLConnection httpURLConnection = null;
                URL url = new URL(login_url);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");
                    httpURLConnection.setRequestMethod("DELETE");
                    Log.i("z", String.valueOf(httpURLConnection.getResponseCode()));

            }
            else {
                Log.i("Z", "making POST " +json);
                // Defined URL  where to send data
                URL url = new URL(login_url);

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( json );
                wr.flush();

                // Get the server response
                BufferedReader reader=null;
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }

                Log.i("z", sb.toString());
                return sb.toString();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return null;
    }

    public static String doGet(String urlToRead) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }

    @Override
    protected void onPreExecute() {
//        alertDialog = new AlertDialog.Builder(context).create();
//        alertDialog.setTitle("Test DB Result");
    }

    @Override
    protected void onPostExecute(String result) {

//        alertDialog.setMessage(result);
//        alertDialog.show();

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    public String getJson() {
        return json;
    }

}
