package com.z.attendance.services;

import android.content.Context;

import com.google.gson.Gson;
import com.z.attendance.models.Base;
import com.z.attendance.models.Clan;
import com.z.attendance.models.EmployeeData;
import com.z.attendance.models.Feed;
import com.z.attendance.models.User;
import com.z.attendance.models.War;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Z on 7/30/2017.
 */

public class CyaServiceImpl implements CyaService {

    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String DELETE = "DELETE";

    private static final String BASES = "bases.php";
    private static final String CLANS = "clans.php";
    private static final String FEED = "feed.php";
    private static final String USERS = "users.php";
    private static final String WARS = "wars.php";

    private static final String WHERE_CLAN_EQUAL = "?clan=";
    private static final String WHERE_ID_EQUAL = "?id=";
    private static final String WHERE_KEY_EQUAL = "?k3y=";
    private static final String WHERE_WAR_EQUAL = "?war=";


    private BackgroundWorker bgWorker;

    public CyaServiceImpl(Context ctx) {
        this.bgWorker = new BackgroundWorker(ctx);
    }


    @Override
    public void deleteBase(Base base) {
        bgWorker.execute(DELETE, BASES + WHERE_WAR_EQUAL + base.getWarId());
    }

    @Override
    public void putBase(Base base) {
        bgWorker.execute(POST, BASES, base.toJson());
    }

    @Override
    public List<Base> getBases(int warId) throws InterruptedException , ExecutionException{
        String result = bgWorker.execute(GET, BASES + WHERE_WAR_EQUAL + warId).get();
        Base[] data = new Gson().fromJson(result, Base[].class);
        return  Arrays.asList(data);
    }

    @Override
    public void putClan(Clan clan) {
        bgWorker.execute(POST, CLANS, clan.toJson());
    }

    @Override
    public Clan getClan(int id) throws InterruptedException , ExecutionException {
        String result = bgWorker.execute(GET, CLANS + WHERE_ID_EQUAL + id).get();
        Clan[] data = new Gson().fromJson(result, Clan[].class);
        List<Clan> dataList = Arrays.asList(data);
        if (dataList != null && !dataList.isEmpty()) {
            return dataList.get(0);
        }
        return new Clan();
    }

    @Override
    public Clan getClan(String k3y) throws InterruptedException , ExecutionException {
        String result = bgWorker.execute(GET, CLANS + WHERE_KEY_EQUAL + k3y).get();
        Clan[] data = new Gson().fromJson(result, Clan[].class);
        List<Clan> dataList = Arrays.asList(data);
        if (dataList != null && !dataList.isEmpty()) {
            return dataList.get(0);
        }
        return new Clan();
    }

    @Override
    public void deleteFeeds(int warId) {
        bgWorker.execute(DELETE, FEED + WHERE_WAR_EQUAL + warId);
    }

    @Override
    public void putFeed(Feed feed) {
        bgWorker.execute(POST, FEED, feed.toJson());
    }

    @Override
    public List<Feed> getFeeds(int warId) throws InterruptedException , ExecutionException {
        String result = bgWorker.execute(GET, FEED + WHERE_WAR_EQUAL + warId).get();
        Feed[] data = new Gson().fromJson(result, Feed[].class);
        return  Arrays.asList(data);
    }

    @Override
    public void putUser(User user) {
        bgWorker.execute(POST, USERS, user.toJson());
    }

    @Override
    public User getUser(String id) throws InterruptedException , ExecutionException {
        String result = bgWorker.execute(GET, USERS + WHERE_ID_EQUAL + id).get();
        User[] data = new Gson().fromJson(result, User[].class);
        List<User> dataList = Arrays.asList(data);
        if (dataList != null && !dataList.isEmpty()) {
            return dataList.get(0);
        }
        return new User();
    }

    @Override
    public List<User> getUsers(int clanId) throws InterruptedException , ExecutionException {
        String result = bgWorker.execute(GET, USERS + WHERE_CLAN_EQUAL + clanId).get();
        User[] data = new Gson().fromJson(result, User[].class);
        return Arrays.asList(data);
    }

    @Override
    public void deleteWar(int id) {
        bgWorker.execute(DELETE, WARS + WHERE_ID_EQUAL + id);
    }

    @Override
    public void putWar(War war) {
        bgWorker.execute(POST, WARS, war.toJson());
    }

    @Override
    public War getWar(int id) throws InterruptedException , ExecutionException  {
        String result = bgWorker.execute(GET, WARS + WHERE_ID_EQUAL + id).get();
        War[] data = new Gson().fromJson(result, War[].class);
        List<War> dataList = Arrays.asList(data);
        if (dataList != null && !dataList.isEmpty()) {
            return dataList.get(0);
        }
        return new War();
    }
}
