CREATE TABLE `base` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `rank` int(11) NOT NULL,
 `townHallLevel` int(11) NOT NULL,
 `warId` int(11) NOT NULL,
 `numStars` int(11) NOT NULL,
 `calledBy` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1

CREATE TABLE `clan` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `currentWarId` int(11) DEFAULT NULL,
 `k3y` varchar(64) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1

CREATE TABLE `feed` (
 `warId` int(11) NOT NULL,
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `text` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1

CREATE TABLE `user` (
 `facebookId` varchar(255) NOT NULL,
 `name` varchar(255) NOT NULL,
 `clanId` int(11) NOT NULL,
 `isAdmin` tinyint(1) NOT NULL,
 `numCallsThisWar` int(11) NOT NULL,
 PRIMARY KEY (`facebookId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

CREATE TABLE `war` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `against` varchar(255) NOT NULL,
 `numBases` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin